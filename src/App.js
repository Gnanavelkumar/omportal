import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';
import "bootstrap/dist/css/bootstrap.css";
/** Layouts **/
import MainLayout from "./components/mainlayout";
import InnerLayout from "./components/innerlayout";

/** Components **/
import VendorTable from './components/vender-page';
import StocksMain from './components/stocks/stocks-main';
import ProductsMain from './components/stocks/stocks-products';
import NewProduct from './components/stocks/new-product';
import AllComponents from './components/all-components';
import NewPurchaseProduct from './components/stocks/purchase-new';

import Dashboard from './components/dashboard';

import VendorListsTable from './components/vendors/vender-page-main';
import AddVendor from './components/vendors/addvendor';

import OrderListsTable from './components/order/order-page-main';
import OrderReturnTable from './components/order/order-return-page';
import OrderInvoiceTable from './components/order/order-invoice-page';
import OrderPopUp from './components/order/order-all-pop-up';

import SalesBreakDowns from './components/reports/salesbreakdowns';
import ProductPerformance from './components/reports/product-performance';
import VendorBreakDowns from './components/reports/vendor-breakdown';


import PromotionsMain from './components/promotions/promotions-main';
import NewPromotions from './components/promotions/new-promotions';
import NewProductPromotions from './components/promotions/new-product-promotions';

import Signin from './components/signin';
import Forgot from './components/forgot-password';
/*
   App
 */
class App extends Component {
  render() {
      return (
          <Router>
              <Switch>
                  <Route exact path="/">
                      <Redirect to="/Signin" />
                  </Route>
                  <MainLayout path="/Signin" component={Signin} />
                  <MainLayout path="/forgotpassword" component={Forgot} />
                  
                  <InnerLayout path="/purchase" exact  component={VendorTable}/>
                  <InnerLayout path="/stocks" exact  component={StocksMain}/>
                  <InnerLayout path="/products" exact  component={ProductsMain}/>
                  <InnerLayout path="/newproduct" exact  component={NewProduct}/>
                  <InnerLayout path="/newproductpurchase" exact  component={NewPurchaseProduct}/>

                  <InnerLayout path="/VendorGridView" exact  component={VendorListsTable}/>
                  <InnerLayout path="/AddNewVendor" exact  component={AddVendor}/>

                  <InnerLayout path="/OrderGridView" exact  component={OrderListsTable}/>
                  <InnerLayout path="/ReturnGridView" exact  component={OrderReturnTable}/>
                  <InnerLayout path="/InvoiceGridView" exact  component={OrderInvoiceTable}/>
                  <InnerLayout path="/OrderAllPopUp" exact  component={OrderPopUp}/>

                  <InnerLayout path="/" exact  component={Dashboard}/>
                  
                  <InnerLayout path="/SalesBreakDowns" exact  component={SalesBreakDowns}/>
                  <InnerLayout path="/ProductPerformance" exact  component={ProductPerformance}/>
                  <InnerLayout path="/VendorBreakDowns" exact  component={VendorBreakDowns}/>


                  <InnerLayout path="/promotions" exact  component={PromotionsMain}/>
                  <InnerLayout path="/new-promotions" exact  component={NewPromotions}/>
                  <InnerLayout path="/new-productpromotions" exact  component={NewProductPromotions}/>

                  <InnerLayout path="/all" exact  component={AllComponents}/>
              </Switch>
          </Router>
      );
  }
}


export default App;
