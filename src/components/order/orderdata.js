import React from "react";
import Button from 'react-bootstrap/Button';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';

export const columns = [
  {
    name: "Order No",
    selector: "orderno",
    sortable: true,
     cell: row => <span class="cblue">VC0001</span>
  },
 
  {
    name: "Customer ID",
    selector: "customerID",
    sortable: true
    // cell: row = <span class="cblue">PBS001,ALC0751</span>
  },
  {
    name: "Mobile No",
    selector: "mobile no",
    sortable: true,
    cell: row => <span class="">7987564874</span>
  },
  {
    name: "Email",
    selector: "email",
    sortable: true
  },
  {
    name: "Ship Address",
    selector: "=ship address",
    sortable: true,
    cell: row => <span className="">10 Downing Street</span>
  },
  
  {
    name: "Product Name",
    selector: "product name",
    sortable: true,
    cell: row => <span className="">Phopio</span>
    
  },
   
  {
    name: "SKU",
    selector: "sku",
    sortable: true
    
  },
  {
    name: "Qty",
    selector: "qty",
    sortable: true
    
  },

  {
    name: "Total Amount",
    selector: "total amount",
    sortable: true,
    cell: row => <span className="">£1200</span>
    
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
    cell: row => <span className="processing">Processing</span>
    
  },
  {
    name: "Created Date",
    selector: "created date",
    sortable: true,
    cell: row => <span className="">02/04/2020</span>
    
  },
 
  {
    name: " Ship by Date",
    selector: " ship by date",
    sortable: true,
    cell: row => <span className="">02/04/2020</span>
    
  },
  {
    name: "Delivered Date",
    selector: "delivered date",
    sortable: true,
    cell: row => <span className="">02/04/2020</span>
    
  },
  
  {
    name: "Action",
    selector: "action",
    sortable: false,
    cell: row => <OverlayTrigger placement="top" overlay={<Tooltip id="tooltip">View Details</Tooltip>}><span class="actioneround"><span class="icon_view" aria-hidden="true"></span></span></OverlayTrigger>,
  }
];

export const data = [
  {
    orderno: 'VC0001',
    //supportbrand: "Phobio,Alcatel",
    //supportproducts: "PBS001,ALC0751",
    customerID: "SGF001KL",
    mobilenumber: "7987564874",
    email: 'Micromax@mmc.com',
    shipaddress: '10 Downing Street',
    productname: 'Phopio',
    sku: "ALCO751",
    qty: "10",
    totalamount: "£1200",
    status: "span className='processing'>Processing</span>",
    createddate: "02/04/2020",
    shipbydate: "02/04/2020",
    delivereddate: "02/04/2020",
    action: <OverlayTrigger placement="top" overlay={<Tooltip id="tooltip">View Details</Tooltip>}><span class="actioneround"><span class="icon_view" aria-hidden="true"></span></span></OverlayTrigger>,
    // cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    orderno: 'VC0001',
    //supportbrand: "Phobio,Alcatel",
    //supportproducts: "PBS001,ALC0751",
    customerID: "SGF001KL",
    mobilenumber: "7987564874",
    email: 'Micromax@mmc.com',
    shipaddress: '10 Downing Street',
    productname: 'Phopio',
    sku: "ALCO751",
    qty: "10",
    totalamount: "£1200",
    status: "span className='processing'>Processing</span>",
    createddate: "02/04/2020",
    shipbydate: "02/04/2020",
    delivereddate: "02/04/2020",
    action: <OverlayTrigger placement="top" overlay={<Tooltip id="tooltip">View Details</Tooltip>}><span class="actioneround"><span class="icon_view" aria-hidden="true"></span></span></OverlayTrigger>,
    // cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    orderno: 'VC0001',
    //supportbrand: "Phobio,Alcatel",
    //supportproducts: "PBS001,ALC0751",
    customerID: "SGF001KL",
    mobilenumber: "7987564874",
    email: 'Micromax@mmc.com',
    shipaddress: '10 Downing Street',
    productname: 'Phopio',
    sku: "ALCO751",
    qty: "10",
    totalamount: "£1200",
    status: "span className='processing'>Processing</span>",
    createddate: "02/04/2020",
    shipbydate: "02/04/2020",
    delivereddate: "02/04/2020",
    action: <OverlayTrigger placement="top" overlay={<Tooltip id="tooltip">View Details</Tooltip>}><span class="actioneround"><span class="icon_view" aria-hidden="true"></span></span></OverlayTrigger>,
    // cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    orderno: 'VC0001',
    //supportbrand: "Phobio,Alcatel",
    //supportproducts: "PBS001,ALC0751",
    customerID: "SGF001KL",
    mobilenumber: "7987564874",
    email: 'Micromax@mmc.com',
    shipaddress: '10 Downing Street',
    productname: 'Phopio',
    sku: "ALCO751",
    qty: "10",
    totalamount: "£1200",
    status: "span className='processing'>Processing</span>",
    createddate: "02/04/2020",
    shipbydate: "02/04/2020",
    delivereddate: "02/04/2020",
    action: <OverlayTrigger placement="top" overlay={<Tooltip id="tooltip">View Details</Tooltip>}><span class="actioneround"><span class="icon_view" aria-hidden="true"></span></span></OverlayTrigger>,
    // cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    orderno: 'VC0001',
    //supportbrand: "Phobio,Alcatel",
    //supportproducts: "PBS001,ALC0751",
    customerID: "SGF001KL",
    mobilenumber: "7987564874",
    email: 'Micromax@mmc.com',
    shipaddress: '10 Downing Street',
    productname: 'Phopio',
    sku: "ALCO751",
    qty: "10",
    totalamount: "£1200",
    status: "span className='processing'>Processing</span>",
    createddate: "02/04/2020",
    shipbydate: "02/04/2020",
    delivereddate: "02/04/2020",
    action: <OverlayTrigger placement="top" overlay={<Tooltip id="tooltip">View Details</Tooltip>}><span class="actioneround"><span class="icon_view" aria-hidden="true"></span></span></OverlayTrigger>,
    // cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  }
 ];
