import React, { Component,  useState } from "react";
import { Link } from "react-router-dom";
import { Button ,Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';


function OrderPopUp()  {

 
const [show, setShow] = useState(false);
const [show2, setShow2] = useState(false);
const [show3, setShow3] = useState(false);
const [show4, setShow4] = useState(false);
const [show5, setShow5] = useState(false);
const [show6, setShow6] = useState(false);
const [show7, setShow7] = useState(false);

const handleClose = () => setShow(false);
const handleShow = () => setShow(true);

const handleClose2 = () => setShow2(false);
const handleShow2 = () => setShow2(true);

const handleClose3 = () => setShow3(false);
const handleShow3 = () => setShow3(true);

const handleClose4 = () => setShow4(false);
const handleShow4 = () => setShow4(true);

const handleClose5 = () => setShow5(false);
const handleShow5 = () => setShow5(true);

const handleClose6 = () => setShow6(false);
const handleShow6 = () => setShow6(true);

const handleClose7 = () => setShow7(false);
const handleShow7 = () => setShow7(true);

        return (
<>
    <div className="row clearfix">
             {/* <div className="succes-bar green">
                <span><img src="images/succestick.svg" alt="" className="logo-right-suc" />Micromax Informatics details updated successfully<img src="images/close-pop.svg" alt="user" className="logo-close-pass mr-0" /></span>
            </div> */}
      <div className="col-sm-12 p-0">
          <div className="user-icon-right-head">
            <h4>Orders All Popup</h4>
            
        </div>
        <div className="ctables">
        <div className="text-left">
        <button type="button" className="btn btn-primary mr-2" onClick={handleShow}>Update Status</button>
          <button type="button" className="btn btn-primary mr-2" onClick={handleShow2}>Product Update Status</button> 
          <button type="button" className="btn btn-primary mr-2" onClick={handleShow3}>Delete</button> 
          <button type="button" className="btn btn-primary mr-2" onClick={handleShow4}>Refund </button> 
          <button type="button" className="btn btn-primary mr-2" onClick={handleShow5}>Dispach</button> 
          <button type="button" className="btn btn-primary mr-2" onClick={handleShow6}>Product </button>
          <button type="button" className="btn btn-primary " onClick={handleShow7}>Order </button>
          </div>
        </div>
      </div>
      
    </div>
    <Modal size="md" className="update-pop-up"show={show} onHide={handleClose}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Update Status</Modal.Title>
  </Modal.Header>
  <Modal.Body>
      <div className="row clearfix align-items-center">
            <div className="col-sm-3">
              <img src="images/tringle.svg" className="img-fluid"/> 
            </div>
            <div className="col-sm-9">
                <div className="rig-cnt-txt-ban">
                      <h3>Return ID: VC0001<span className="dispatched ml-2">Refund Initiated</span></h3>
                      <h4><strong>SKU: ALCO751</strong></h4>
                      <h4>Created Date: 02/04/2020</h4>
                </div>
            </div>
      </div>
      <div className="row clearfix ">
            <div className="col-sm-12">
            <div class="form-group  "><label for="email" class=" col-form-label ">Return Reason</label><div class=""><textarea class="form-control custom-form ar-txt-bg" rows="2" id="comment" placeholder="“This dial-pad is not working properly”"></textarea>
            <span className="cust-id-txt">by customer ID</span>
            </div></div>
            
            </div>
      </div>
      <div className="row clearfix ">
        <div className="col-sm-12">
            <div class="form-check cus-form-check checked purple">
              <input class="form-check-input upd-file" type="radio" name="exampleRadios" id="exampleRadios1" value='upd-file' data-id="upd-file"/>
              <label class="form-check-label" for="exampleRadios1">
              Initiate Refund
            </label>   
            <p>Verify the received product quality, If product quality is good initiate refund.</p>
            </div>  
        </div> 

        <div className="col-sm-12">
            <div class="form-check cus-form-check checked ">
              <input class="form-check-input upd-file" type="radio" name="exampleRadios" id="exampleRadios1" value='upd-file' data-id="upd-file"/>
              <label class="form-check-label" for="exampleRadios1">
              Cancel Refund
            </label>   
            <p>Verify the received product quality, If product quality not good / damaged, Enter the reason and cancel the refund.</p>
            {/* <textarea placeholder="Enter reason of the product quality"></textarea> */}
            </div>  
        </div>  
    </div> 
      
    
    
   
    

  </Modal.Body>
  <Modal.Footer>
    <Button variant="popcuslink bdr">Cancel</Button>
    <Button variant="primary popcus refund-pop-btm-btn-bg" onClick={handleClose}>Save Status & Send Notification</Button>
  </Modal.Footer>
</Modal>

<Modal size="md" show={show2} onHide={handleClose2}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Upload Status</Modal.Title>
  </Modal.Header>
  <Modal.Body>
  <div className="row clearfix align-items-center">
            <div className="col-sm-3">
              <img src="images/tringle.svg" className="img-fluid"/> 
            </div>
            <div className="col-sm-9">
                <div className="rig-cnt-txt-ban">
                      <h3>Product Order ID: VC0006546564161<span className="canceled ml-2">Cancelled</span></h3>
                      <h4><strong>SKU: ALCO751</strong></h4>
                      <h4>Created Date: 02/04/2020</h4>
                      <h4>Vendor: Micromax Informatics</h4>
                </div>
            </div>
      </div>
      <br/>
      <div className="row clearfix ">
        <div className="col-sm-12">
            <div class="form-check cus-form-check checked ">
              <input class="form-check-input upd-file" type="radio" name="exampleRadios" id="exampleRadios1" value='upd-file' data-id="upd-file"/>
              <label class="form-check-label" for="exampleRadios1">
              Order Received
            </label>   
            <p>Ordered product is received, Please update the status of the product.</p>
            </div>  
        </div> 

        <div className="col-sm-12">
            <div class="form-check cus-form-check checked red">
              <input class="form-check-input upd-file" type="radio" name="exampleRadios" id="exampleRadios1" value='upd-file' data-id="upd-file"/>
              <label class="form-check-label" for="exampleRadios1">
              Order Canceled
            </label>   
            <p>If order canceled, please update the status of the product not received.</p>
            <textarea placeholder="Enter reason of the product quality"></textarea>
            </div>  
        </div>  
    </div>  


  </Modal.Body>
  <Modal.Footer>
     <Button variant="popcuslink bdr">Cancel</Button>
    <Button variant="primary popcus cancel-pop-btm-btn-bg" onClick={handleClose2}>Save Status & Send Notification</Button>
    
  </Modal.Footer>
</Modal>
<Modal size="sm" className=" delete-bg"show={show3} onHide={handleClose3}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Delete</Modal.Title>
  </Modal.Header>
  <Modal.Body>
  <div class="row clear-fix">
    <div class="col-sm-12">
      <p class="mb-3">Are you sure you want to cancel? Your status update will not be saved.</p>
    </div>
   
</div>


  </Modal.Body>
  <Modal.Footer>
    <Button variant="popcuslink bdr">cancel</Button>
    <Button variant="primary popcus yes-bg" onClick={handleClose3}>Yes</Button>
  </Modal.Footer>
</Modal>


<Modal size="md" show={show4} onHide={handleClose4}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Upload Status</Modal.Title>
  </Modal.Header>
  <Modal.Body>
  <div className="row clearfix align-items-center">
            <div className="col-sm-3">
              <img src="images/tringle.svg" className="img-fluid"/> 
            </div>
            <div className="col-sm-9">
                <div className="rig-cnt-txt-ban">
                      <h3>Return ID: VC0001<span className="dispatched ml-2">Refund Initiated</span></h3>
                      <h4><strong>SKU: ALCO751</strong></h4>
                      <h4>Refund Initiated Date: 02/04/2020</h4>
                      {/* <h4>Vendor: Micromax Informatics</h4> */}
                </div>
            </div>
      </div>
      <br/>
      <div className="row clearfix ">
        <div className="col-sm-12 refun-sm-hes-txt">
           <h3 className=""> Refund is completed?</h3>
            <p>Refund process completed. Please complete the return status.</p>
        </div> 
    </div>  


  </Modal.Body>
  <Modal.Footer>
  <Button variant="popcuslink bdr">Cancel</Button>
    <Button variant="primary popcus complete-pop-btm-btn-bg" onClick={handleClose4}>Refund Completed</Button>
  </Modal.Footer>
</Modal>
<Modal size="md" show={show5} onHide={handleClose5}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Upload Status</Modal.Title>
  </Modal.Header>
  <Modal.Body>
  <div className="row clearfix align-items-center">
            <div className="col-sm-3">
              <img src="images/tringle.svg" className="img-fluid"/> 
            </div>
            <div className="col-sm-9">
                <div className="rig-cnt-txt-ban">
                      <h3>Order No: VC0001<span className="dispatched ml-2">Dispatched</span></h3>
                      <h4><strong>SKU: ALCO751</strong></h4>
                      <h4>Created Date: 02/04/2020</h4>
                      {/* <h4>Vendor: Micromax Informatics</h4> */}
                </div>
            </div>
      </div>
      <br/>
      <div className="row clearfix ">
        <div className="col-sm-12 refun-sm-hes-txt">
           <h3 className=""> Dispatch is Started?</h3>
            <p>If dispatch process is started, please check and update here.</p>
        </div> 
    </div>  


  </Modal.Body>
  <Modal.Footer>
  <Button variant="popcuslink bdr">Cancel</Button>
    <Button variant="primary popcus refund-pop-btm-btn-bg" onClick={handleClose5}>Save Status & Send Notification</Button>
  </Modal.Footer>
</Modal>
<Modal size="md" show={show6} onHide={handleClose6}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Upload Status</Modal.Title>
  </Modal.Header>
  <Modal.Body>
  <div className="row clearfix align-items-center">
            <div className="col-sm-3">
              <img src="images/tringle.svg" className="img-fluid"/> 
            </div>
            <div className="col-sm-9">
                <div className="rig-cnt-txt-ban">
                      <h3>Order No: VC0001<span className="ordered ml-2">Delivered</span></h3>
                      <h4><strong>SKU: ALCO751</strong></h4>
                      <h4>Created Date: 02/04/2020</h4>
                      {/* <h4>Vendor: Micromax Informatics</h4> */}
                </div>
            </div>
      </div>
      <br/>
      <div className="row clearfix ">
        <div className="col-sm-12 refun-sm-hes-txt">
           <h3 className=""> Product is delivered?</h3>
            <p>If product is delivered, please check and update here.</p>
        </div> 
    </div>  


  </Modal.Body>
  <Modal.Footer>
  <Button variant="popcuslink bdr">Cancel</Button>
    <Button variant="primary popcus complete-pop-btm-btn-bg" onClick={handleClose6}>Save Status & Send Notification</Button>
  </Modal.Footer>
</Modal>
<Modal size="md" show={show7} onHide={handleClose7}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Upload Status</Modal.Title>
  </Modal.Header>
  <Modal.Body>
  <div className="row clearfix align-items-center">
            <div className="col-sm-3">
              <img src="images/tringle.svg" className="img-fluid"/> 
            </div>
            <div className="col-sm-9">
                <div className="rig-cnt-txt-ban">
                      <h3>Order No: VC0001<span className="dispatched ml-2">Dispatched</span></h3>
                      <h4><strong>SKU: ALCO751</strong></h4>
                      <h4>Created Date: 02/04/2020</h4>
                      {/* <h4>Vendor: Micromax Informatics</h4> */}
                </div>
            </div>
      </div>
      <br/>
      <div className="row clearfix ">
        <div className="col-sm-12 ">
          <div className="row clearfix refun-sm-hes-txt">
              <div className="col-sm-3">
                <p className="mb-1">Mobile No</p>
                <h3 className=""> 7236726372</h3>
            </div>
              <div className="col-sm-4">
              <p className="mb-1">Email</p>
              <h3 className=""> micromax@mmc.com</h3>
              </div>
              <div className="col-sm-5">
              <p className="mb-1">Ship Address</p>
              <h3 className=""> 10 Downing Street, LONDON, SW1A 2AA</h3>
              </div>
          </div>
          <hr/>
          <div className="row clearfix refun-sm-hes-txt">
              <div className="col-sm-3">
                <p className="mb-1">Product Name</p>
                <h3 className=""> Phopio</h3>
            </div>
              <div className="col-sm-4">
                <div className="row">
                    <div className="col-sm-6">
                        <p className="mb-1">SKU</p>
                      <h3 className=""> ALCO751</h3>
                    </div>
                    <div className="col-sm-6">
                        <p className="mb-1">Qty</p>
                      <h3 className=""> 10</h3>
                    </div>
                </div>
              </div>
              <div className="col-sm-5">
              <p className="mb-1">Total Amount</p>
              <h3 className=""> £1200</h3>
              </div>
          </div>
          <hr/>
          <div className="row clearfix refun-sm-hes-txt">
              <div className="col-sm-3">
                <p className="mb-1">Ship by Date</p>
                <h3 className=""> 02/04/2020</h3>
            </div>
              <div className="col-sm-4">
                    <p className="mb-1">Delivery Date</p>
                  <h3 className=""> 02/04/2020</h3>
              </div>
            
          </div>
        </div> 
    </div> 


  </Modal.Body>
  {/* <Modal.Footer>
  <Button variant="popcuslink bdr">Cancel</Button>
    <Button variant="primary popcus complete-pop-btm-btn-bg" onClick={handleClose6}>Save Status & Send Notification</Button>
  </Modal.Footer> */}
</Modal>
</>
	);
}


export default OrderPopUp;
