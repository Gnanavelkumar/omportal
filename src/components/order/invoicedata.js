import React from "react";
import Button from 'react-bootstrap/Button'
export const columns = [
 
  {
    name: "Invoice No",
    selector: "invoiceno",
    sortable: true,
    cell: row => <span class="">VC001</span>
  },
 
  {
    name: "Order No",
    selector: "orderno",
    sortable: true,
    cell: row => <span class="">PG05</span>
  },
  {
    name: "Order Amount",
    selector: "orderamount",
    sortable: true,
    cell: row => <span class="">£21.64</span>
  },
  {
    name: "Shiping Address",
    selector: "shipingaddress",
    sortable: true,
    cell: row => <span class="">10 Downing Street, LONDON, SW1A 2AA</span>
  },
  {
    name: "Billing Address",
    selector: "billingaddress",
    sortable: true,
    cell: row => <span class="">10 Downing Street, LONDON, SW1A 2AA</span>
  },
  {
    name: "Discount (%)",
    selector: "discount",
    sortable: true,
    cell: row => <span class="">10 %</span>
  },
  {
    name: "Shipment Cost",
    selector: "shipmentcost",
    sortable: true,
    cell: row => <span class="">£2.64</span>
  },
  
  
  {
    name: "Payment Mode",
    selector: "paymentmode",
    sortable: true,
    cell: row => <span class="">Credit Card</span>
    
  },
  {
    name: "Invoice",
    selector: "invoice",
    sortable: false,
    cell: row =><div class="updedimg-invoice dropup custom-drop-over">    <img src="images/upd-invoice.PNG" />  </div>,
  }
];

export const data = [
  {
   
    invoiceno: "VC001",
    orderno: 'PG05',
    orderamount: "£21.64",
    shipingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    billingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    discount: '10%',
    shipmentcost: "£2.64",
    paymentmode: "Credit Card",
    invoice:'<div class="updedimg-invoice dropup custom-drop-over"><img src="images/upd-invoice.PNG" /></div>'
    // cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    invoiceno: "VC001",
    orderno: 'PG05',
    orderamount: "£21.64",
    shipingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    billingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    discount: '10%',
    shipmentcost: "£2.64",
    paymentmode: "Credit Card",
    invoice:'<div class="updedimg-invoice dropup custom-drop-over"><img src="images/upd-invoice.PNG" /></div>'
  },
  {
    invoiceno: "VC001",
    orderno: 'PG05',
    orderamount: "£21.64",
    shipingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    billingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    discount: '10%',
    shipmentcost: "£2.64",
    paymentmode: "Credit Card",
    invoice:'<div class="updedimg-invoice dropup custom-drop-over"><img src="images/upd-invoice.PNG" /></div>'
  },
  {
    invoiceno: "VC001",
    orderno: 'PG05',
    orderamount: "£21.64",
    shipingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    billingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    discount: '10%',
    shipmentcost: "£2.64",
    paymentmode: "Credit Card",
    invoice:'<div class="updedimg-invoice dropup custom-drop-over"><img src="images/upd-invoice.PNG" /></div>'
  },
  {
    invoiceno: "VC001",
    orderno: 'PG05',
    orderamount: "£21.64",
    shipingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    billingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    discount: '10%',
    shipmentcost: "£2.64",
    paymentmode: "Credit Card",
    invoice:'<div class="updedimg-invoice dropup custom-drop-over"><img src="images/upd-invoice.PNG" /></div>'
    
  },
  {
    invoiceno: "VC001",
    orderno: 'PG05',
    orderamount: "£21.64",
    shipingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    billingaddress: "10 Downing Street, LONDON, SW1A 2AA",
    discount: '10%',
    shipmentcost: "£2.64",
    paymentmode: "Credit Card",
    invoice:'<div class="updedimg-invoice dropup custom-drop-over"><img src="images/upd-invoice.PNG" /></div>'
  }];
