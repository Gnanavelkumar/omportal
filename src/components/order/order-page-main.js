import React, { Component,  useState } from "react";
import { Link } from "react-router-dom";
import { Button ,Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';
import DataTable from "react-data-table-component";
import DataTableExtensions from 'react-data-table-component-extensions';
import {columns, data} from "../order/orderdata";
import Select from 'react-select';

function OrderListsTable()  {

  const options = [
    { value: 'All', label: 'All' },
    { value: 'Processing', label: 'Processing' },
    { value: 'Dispatched', label: 'Dispatched' },
    { value: 'Delivered', label: 'Delivered' }, 
    { value: 'Cancelled', label: 'Cancelled' } 
  ];
  

const [hideDirector, setHideDirector] = React.useState(false);
    const tableData = {
        columns,
        data
      };

    const BootyCheckbox = React.forwardRef(({ onClick, ...rest }, ref) => (
      <div className="custom-control custom-checkbox">
        <input
          type="radio"
          className="custom-control-input"
          ref={ref}
          {...rest}
        />
        <label className="custom-control-label" onClick={onClick} />
      </div>
    ));

    const handleChange = (state) => {
    // You can use setState or dispatch with something like Redux so we can use the retrieved data
    console.log('Selected Rows: ', state.selectedRows);
    };
    
        return (
<>
    <div className="row clearfix">
       {/* <div className="succes-bar green">
                <span><img src="images/succestick.svg" alt="" className="logo-right-suc" />Micromax Informatics details updated successfully<img src="images/close-pop.svg" alt="user" className="logo-close-pass mr-0" /></span>
            </div> */}
      <div className="col-sm-12 p-0">
        <div className="user-icon-right-head">
            <h4>Orders <span className="outof">(12 of 12)</span></h4>
            {/* <div className="text-right"><button type="button" className="btn btn-primary mr-2">Add Product</button> <button type="button" className="btn btn-outline" onClick={handleShow}><span className="icon_upload"></span>Bulk Upload</button></div> */}
            
           
        </div>
        <div className="ctables">
        <div className="filterrow">
                    <div className="col-sm-3 col-md-3">
                      <label className="cusselect-label">Status</label> <Select options = {options} className="cusselect" />
                    </div>
                    {/* <div className="col-sm-3 col-md-3">
                      <label className="cusselect-label">Stocks</label> <Select options = {options2} className="cusselect"/>
                    </div> */}
                    <div className="col-sm-6 col-md-6 text-right pr-0">
                      <div className="fillterbtn">
                        <Button className="btn-blue-border" onClick={() => setHideDirector(!hideDirector)}>Download Report</Button>
                        <a href="" class="dtbtn-cus" data-toggle="modal" data-target="#delexport"><span className="icon_setting"></span> </a>
                      </div>
                    </div>
                  </div>

        <DataTableExtensions {...tableData}>
          <DataTable
            columns={columns}
            data={data}
            defaultSortField="vendorname"
            pagination
            selectableRows
            filtering
            selectableRowsComponent={BootyCheckbox}
          />
          </DataTableExtensions>
        </div>
      </div>
      
    </div>
    
</>
	);
}


export default OrderListsTable;
