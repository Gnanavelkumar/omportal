import React from "react";
import Button from 'react-bootstrap/Button'
export const columns = [
  {
    name: "Return ID",
    selector: "returnID",
    sortable: true,
    cell: row => <span class="cblue">REC001</span>
  },
  {
    name: "Invoice No",
    selector: "invoiceno",
    sortable: true,
    cell: row => <span class="">VC001</span>
  },
  {
    name: "Order No",
    selector: "orderno",
    sortable: true,
    cell: row => <span class="">PG05</span>
  },
  {
    name: "Product Name",
    selector: "productname",
    sortable: true,
    cell: row => <span class="">Phobio</span>
  },
  {
    name: "Product Type",
    selector: "producttype",
    sortable: true,
    cell: row => <span class="">Deskphone-Wired</span>
  },
  {
    name: "Order Amount",
    selector: "orderamount",
    sortable: true,
    cell: row => <span class="">£2.64</span>
  },
  
  {
    name: "Payment Mode",
    selector: "paymentmode",
    sortable: true,
    cell: row => <span class="">Credit Card</span>
    
  },
  {
    name: "Action",
    selector: "action",
    sortable: false,
    cell: row => <span className="canceled">Cancelled</span>,
  }
];

export const data = [
  {
    returnID: 'REC001',
    invoiceno: "VC001",
    orderno: "PG05",
    productname: "Phobio",
    producttype: 'Deskphone-Wired',
    orderamount: "£2.64",
    paymentmode: "Credit Card",
    action:'<span className="canceled">Cancelled</span>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    returnID: 'REC001',
    invoiceno: "VC001",
    orderno: "PG05",
    productname: "Phobio",
    producttype: 'Deskphone-Wired',
    orderamount: "£2.64",
    paymentmode: "Credit Card",
    action:'<span className="canceled">Cancelled</span>'
  },
  {
    returnID: 'REC001',
    invoiceno: "VC001",
    orderno: "PG05",
    productname: "Phobio",
    producttype: 'Deskphone-Wired',
    orderamount: "£2.64",
    paymentmode: "Credit Card",
    action:'<span className="canceled">Cancelled</span>'
  },
  {
    returnID: 'REC001',
    invoiceno: "VC001",
    orderno: "PG05",
    productname: "Phobio",
    producttype: 'Deskphone-Wired',
    orderamount: "£2.64",
    paymentmode: "Credit Card",
    action:'<span className="canceled">Cancelled</span>'
  },
  {
    returnID: 'REC001',
    invoiceno: "VC001",
    orderno: "PG05",
    productname: "Phobio",
    producttype: 'Deskphone-Wired',
    orderamount: "£2.64",
    paymentmode: "Credit Card",
    action:'<span className="canceled">Cancelled</span>'
    
  },
  {
    returnID: 'REC001',
    invoiceno: "VC001",
    orderno: "PG05",
    productname: "Phobio",
    producttype: 'Deskphone-Wired',
    orderamount: "£2.64",
    paymentmode: "Credit Card",
    action:'<span className="canceled">Cancelled</span>'
  }];
