import React, { Component } from "react";
import { Link } from "react-router-dom";
import {Nav, NavItem, Navbar, Button, Accordion, Card} from 'react-bootstrap';

class LeftMenu extends Component {
  render() {
    return (
<>
    
<Accordion defaultActiveKey="0" className="left-tab-side-bar">
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="0">
        <span className="icon_dashboard"></span> Dashboard
      </Accordion.Toggle>
    </Card.Header>
  </Card>
  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="1" className="navlink selected">
        <span className="icon_stock"></span> Stocks
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="1">
      <Card.Body>          
        <ul>
            <li className="active"><Link to="">Purchases</Link></li>
            <li><Link to="">Products</Link></li>
        </ul>
      </Card.Body>
    </Accordion.Collapse>
  </Card>

  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="2">
        <span className="icon_vendor"></span> Vendors
      </Accordion.Toggle>
    </Card.Header>
  </Card>

  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="3" className="navlink">
        <span className="icon_promotion"></span> Promotions
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="3">
      <Card.Body>
        <ul>
            <li><Link to="">Purchases</Link></li>
            <li><Link to ="">Products</Link></li>
        </ul>
          
      </Card.Body>
    </Accordion.Collapse>
  </Card>

  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="4" className="navlink">
        <span className="icon_order"></span> Orders
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="4">
      <Card.Body>          
        <ul>
            <li><Link to="">Orders</Link></li>
            <li><Link to ="">Invoice</Link></li>
            <li><Link to ="">Returns</Link></li>
        </ul>
      </Card.Body>
    </Accordion.Collapse>
  </Card>

  <Card>
    <Card.Header>
      <Accordion.Toggle as={Button} variant="link" eventKey="5" className="navlink">
        <span className="icon_file"></span> Reports
      </Accordion.Toggle>
    </Card.Header>
    <Accordion.Collapse eventKey="5">
      <Card.Body>          
        <ul>
            <li><Link to="">Sales Breakdown</Link></li>
            <li><Link to ="">Product Performance</Link></li>
            <li><Link to ="">Vendor Breakdown</Link></li>
        </ul>
      </Card.Body>
    </Accordion.Collapse>
  </Card>

</Accordion>


    
</>
    );
  }
}

export default LeftMenu;
