import React from "react";
import Button from 'react-bootstrap/Button'



export const columns = [
  {
    name: "Customer ID",
    selector: "productid",
    sortable: true
  },
  {
    name: "Customer Location",
    selector: "vendor",
    sortable: true
  },
  {
    name: "Orders Placed",
    selector: "products",
    sortable: true
  },
  {
    name: "Successful Orders",
    selector: "sku",
    sortable: true
  },
  {
    name: "Net Sales",
    selector: "lastupdate",
    sortable: true
  },
  {
    name: "Gross Profit",
    selector: "status",
    sortable: true
  },
  {
    name: "Goods Cost",
    selector: "status",
    sortable: true
  }
];

export const data = [
  {
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },{
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  },
  {
    productid: 'SGHJ00HJLK',
    vendor: "London",
    products: "150",
    sku: "132",
    lastupdate: '£1864',
    status: "£1864",
    invoice: "£1864"
  }];
