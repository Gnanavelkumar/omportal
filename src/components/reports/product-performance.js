import React, { Component,  useState } from "react";
import { Link } from "react-router-dom";
import { Button ,Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';
import DataTable from "react-data-table-component";
import DataTableExtensions from 'react-data-table-component-extensions';
import {columns, data} from "../reports/performance";
import Select from 'react-select';

function ProductPerformance()  {
    
const options = [
  { value: 'All', label: 'All' },
  { value: 'Available', label: 'Available' },
  { value: 'Stock Out', label: 'Stock Out' },
  { value: 'Discontinued', label: 'Discontinued' } 
];
const options2 = [
  { value: 'All', label: 'All' },
  { value: 'Available', label: '<10' },
  { value: 'Stock Out', label: '<30' },
  { value: 'Discontinued', label: '<50' } 
];


  const [hideDirector, setHideDirector] = React.useState(false);
    const tableData = {
        columns,
        data
      };

      

    const BootyCheckbox = React.forwardRef(({ onClick, ...rest }, ref) => (
      <div className="custom-control custom-checkbox">
        <input
          type="radio"
          className="custom-control-input"
          ref={ref}
          {...rest}
        />
        <label className="custom-control-label" onClick={onClick} />
      </div>
    ));
    const handleChange = (state) => {
    // You can use setState or dispatch with something like Redux so we can use the retrieved data
    console.log('Selected Rows: ', state.selectedRows);
    };
    
        return (
<>
  
    
 
  <div className="row clearfix">
    <div className="col-sm-12 p-0">
      <div className="user-icon-right-head">
        <h4>Product Performance</h4>
      </div>
      <div className="ctables newreports">
        <div className="filterrowright new">
          <div className="col-sm-3 col-md-3">
            <h6>
              <label className="cusselect-label">Net Sales</label> 
              <span className="inputsearch">All <span className="icon_down_arrow"></span></span>
            </h6>
            <div className="srvalues">
              <p>Net Sales</p>
              <ul>
                <li><button className=""> &#62; </button></li>
                <li><button className=""> &#61; </button></li>
                <li><button className=""> &#60; </button></li>
              </ul>
              <h5><label><span>£</span> <input type="text" name="" placeholder="Enter Amount"/></label></h5>
              <button className="saves">Save</button>
              <button className="clears">Clear</button>
            </div>
          </div>
          <div className="col-sm-3 col-md-3">
            <h6>
              <label className="cusselect-label">Gross Sales</label> 
              <span className="inputsearch">All <span className="icon_down_arrow"></span></span>
            </h6>
            <div className="srvalues">
              <p>Gross Sales</p>
              <ul>
                <li><button className=""> &#62; </button></li>
                <li><button className=""> &#61; </button></li>
                <li><button className=""> &#60; </button></li>
              </ul>
              <h5><label><span>£</span> <input type="text" name="" placeholder="Enter Amount"/></label></h5>
              <button className="saves">Save</button>
              <button className="clears">Clear</button>
            </div>
          </div>
          <div className="col-sm-6 col-md-6 text-right pr-0">
            <div className="fillterbtn">
              <Button className="btn-blue-border" onClick={() => setHideDirector(!hideDirector)}>Download Report</Button>
              <a href="" class="dtbtn-cus" data-toggle="modal" data-target="#delexport"><span className="icon_setting"></span> </a>
            </div>
          </div>
        </div>
        <DataTableExtensions {...tableData}>
          <DataTable
            columns={columns}
            data={data}
            defaultSortField="productid"
            pagination
            selectableRows
            filtering
            selectableRowsComponent={BootyCheckbox}
          />
        </DataTableExtensions>
      </div>
    </div>
        
  </div>
</>

	);
}


export default ProductPerformance;
