import React from "react";
import Button from 'react-bootstrap/Button'



export const columns = [
  {
    name: "Product Name",
    selector: "productid",
    sortable: true
  },
  {
    name: "Product Type",
    selector: "types",
    sortable: true
  },
  {
    name: "Quantity",
    selector: "qty",
    sortable: true
  },
  {
    name: "Gross Sales",
    selector: "grosssales",
    sortable: true
  },
  {
    name: "Net Sales",
    selector: "lastupdate",
    sortable: true
  }
];

export const data = [
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  },
  {
    productid: 'Phobio',
    type: "Deskphone-wired",
    qty: "150",
    grosssales: "£1864",
    lastupdate: '£1864'
  }
];
