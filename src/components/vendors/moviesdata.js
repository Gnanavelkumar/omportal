import React from "react";
import Button from 'react-bootstrap/Button'
export const columns = [
  {
    name: "Vendor Name",
    selector: "vendorname",
    sortable: true,
    cell: row => <span class="cblue">Micromax Informatics</span>
  },
  {
    name: "Support Brands",
    selector: "supportbrand",
    sortable: true,
    cell: row => <span class="cblue">Phobio,Alcatel</span>
  },
  {
    name: "Support Products",
    selector: "supportproducts",
    sortable: true,
    cell: row => <span class="cblue">PBS001,ALC0751</span>
  },
  {
    name: "Location",
    selector: "location",
    sortable: true,
    cell: row => <span class="cblue">10 Downing Street</span>
  },
  {
    name: "Email",
    selector: "email",
    sortable: true,
    cell: row => <span class="cblue">Micromax@mmc.com</span>
  },
  {
    name: "Mobile Number",
    selector: "=mobilenumber",
    sortable: true,
    cell: row => <span class="cblue">7987564874</span>
  },
  
  {
    name: "Last Update",
    selector: "lastupdate",
    sortable: true,
    cell: row => <span class="cblue">02/04/2020</span>
    
  },
  {
    name: "Action",
    selector: "action",
    sortable: false,
    cell: row => <span className="icon_options actioneround f16"></span>,
  }
];

export const data = [
  {
    vendorname: 'Micromax Informatics',
    supportbrand: "Phobio,Alcatel",
    supportproducts: "PBS001,ALC0751",
    location: "10 Downing Street",
    email: 'Micromax@mmc.com',
    mobilenumber: "7987564874",
    lastupdate: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    vendorname: 'Micromax Informatics',
    supportbrand: "Phobio,Alcatel",
    supportproducts: "PBS001,ALC0751",
    location: "10 Downing Street",
    email: 'Micromax@mmc.com',
    mobilenumber: "7987564874",
    lastupdate: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>'
    
  },
  {
    vendorname: 'Micromax Informatics',
    supportbrand: "Phobio,Alcatel",
    supportproducts: "PBS001,ALC0751",
    location: "10 Downing Street",
    email: 'Micromax@mmc.com',
    mobilenumber: "7987564874",
    lastupdate: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>'
  },
  {
    vendorname: 'Micromax Informatics',
    supportbrand: "Phobio,Alcatel",
    supportproducts: "PBS001,ALC0751",
    location: "10 Downing Street",
    email: 'Micromax@mmc.com',
    mobilenumber: "7987564874",
    lastupdate: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    vendorname: 'Micromax Informatics',
    supportbrand: "Phobio,Alcatel",
    supportproducts: "PBS001,ALC0751",
    location: "10 Downing Street",
    email: 'Micromax@mmc.com',
    mobilenumber: "7987564874",
    lastupdate: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>'
    
  },
  {
    vendorname: 'Micromax Informatics',
    supportbrand: "Phobio,Alcatel",
    supportproducts: "PBS001,ALC0751",
    location: "10 Downing Street",
    email: 'Micromax@mmc.com',
    mobilenumber: "7987564874",
    lastupdate: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>'
  }];
