import React from "react";
import Button from 'react-bootstrap/Button'
export const columns = [

  {
    name: "Brand Name",
    selector: "brandname",
    sortable: true,
    cell: row => <span class="cblue">Phobio,Alcatel</span>
  },
  {
    name: "Product Name/SKU",
    selector: "productname",
    sortable: true,
    cell: row => <span class="cblue">PBS001</span>
  },
  
  {
    name: "Product Type",
    selector: "producttype",
    sortable: true,
    cell: row => <span class="cblue">Deskphone-wireless</span>
    
  },
  {
    name: "Action",
    selector: "action",
    sortable: false,
    cell: row => <div class="text-center de-hvr-chg"><span class="icon_delete gray-delete "></span></div>,
  }
];

export const data = [
  {
   
    brandname: "Phobio,Alcatel",
    productname: "PBS001",
    producttype: "Deskphone-wireless",
    action:'<div class="text-center de-hvr-chg"><span class="icon_delete gray-delete "></span></div>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    brandname: "Phobio,Alcatel",
    productname: "PBS001",
    producttype: "Deskphone-wireless",
    action:'<div class="text-center de-hvr-chg"><span class="icon_delete gray-delete "></span></div>'
    
  },
  {
    brandname: "Phobio,Alcatel",
    productname: "PBS001",
    producttype: "Deskphone-wireless",
    action:'<div class="text-center de-hvr-chg"><span class="icon_delete gray-delete "></span></div>'
  },
  {
    brandname: "Phobio,Alcatel",
    productname: "PBS001",
    producttype: "Deskphone-wireless",
    action:'<div class="text-center de-hvr-chg"><span class="icon_delete gray-delete "></span></div>'
  },
  {
    brandname: "Phobio,Alcatel",
    productname: "PBS001",
    producttype: "Deskphone-wireless",
    action:'<div class="text-center de-hvr-chg"><span class="icon_delete gray-delete "></span></div>'
    
  },
  {
   brandname: "Phobio,Alcatel",
    productname: "PBS001",
    producttype: "Deskphone-wireless",
    action:'<div class="text-center de-hvr-chg"><span class="icon_delete gray-delete "></span></div>'
  }];
