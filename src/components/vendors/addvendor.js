import React, { Component,  useState } from "react";
import { Link } from "react-router-dom";
import { Button ,Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';
import DataTable from "react-data-table-component";
import DataTableExtensions from 'react-data-table-component-extensions';
import {columns, data} from "../vendors/vendorbranddata";
import Select from 'react-select';


function VendorListsTable()  {

    const options = [
        { value: 'All', label: 'All' },
        { value: 'Available', label: 'Available' },
        { value: 'Stock Out', label: 'Stock Out' },
        { value: 'Discontinued', label: 'Discontinued' } 
      ];
  
const [show, setShow] = useState(false);
const [show2, setShow2] = useState(false);

const handleClose = () => setShow(false);
const handleShow = () => setShow(true);

const handleClose2 = () => setShow2(false);
const handleShow2 = () => setShow2(true);

const [hideDirector, setHideDirector] = React.useState(false);
    const tableData = {
        columns,
        data
      };

   

    // const handleChange = (state) => {
    // // You can use setState or dispatch with something like Redux so we can use the retrieved data
    // console.log('Selected Rows: ', state.selectedRows);
    // };
    
        return (
<>
    <div className="row clearfix">
      <div className="col-sm-12 p-0">
      <div class="user-icon-right-head"><ul class="bread-crum-cus-call"><li><a href="#" data-toggle="tab">Vendors</a></li><li><img src="images/bread-arrow.svg" class="bred-arrw" alt=""/></li><li class="active"><a href="#" data-toggle="tab">Add New Vendor</a></li></ul></div>
       
        <div className="centercontent">
                  <div className="contentheadlist">
                      <h5>Add New Product</h5>
                  </div>
                  <div className="center-container">
                  <div className="purchasenew">
                                <div className="row">
                                    <div className="col-md-6">
                                    <h4 className="cal-que-mai-hd-txt">  Vendor Details</h4>    
                                      
                                    <div class="clear-fix ">
                                       
                                            <div className="form-group  ">
                                            <label for="email" class=" col-form-label ">Vendor Name</label>
                                            <div className="">
                                                <input type="password" className="form-control custom-form " placeholder="Enter Vendor Name"/>
                                                
                                            </div>
                                            </div>
                                            <div className="form-group  ">
                                            <label for="email" class=" col-form-label ">Email Address</label>
                                            <div className="">
                                                <input type="password" className="form-control custom-form " placeholder="Enter Email Address"/>
                                                
                                            </div>
                                            </div>
                                            <div className="form-group  ">
                                            <label for="email" class=" col-form-label ">Mobile Number</label>
                                            <div className="">
                                                <input type="password" class="form-control custom-form " placeholder="Enter Mobile Number"/>
                                                
                                            </div>
                                            </div>
                                            <div className="form-group  ">
                                            <label for="email" class=" col-form-label ">Location</label>
                                            <div className="">
                                                <input type="password" class="form-control custom-form " placeholder="Enter Location"/>
                                                
                                            </div>
                                            </div>
                                            <div className="form-group  ">
                                            <label for="email" class=" col-form-label ">Map
                                            <span className="edit-icn-right-align">
                                            <img src="images/edit-pencil.svg" class="bred-arrw-edi-icn" onClick={handleShow2} alt=""/>
                                            </span>
                                            </label>
                                            <div className="">
                                            <img src="images/map_all.png" class="map-icn-bg" alt=""/>
                                                
                                            </div>
                                            </div>
                                                                     
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                    <h4 className="cal-que-mai-hd-txt">  Vendor Distributing Brands</h4> 
                                     <div class="clear-fix ">
                                     <div className="text-right"><button type="button" className="btn btn-primary mr-2" onClick={handleShow}>Add Brand</button> </div>
                                        <div>
                                        <DataTableExtensions {...tableData}>
                                            <DataTable
                                                columns={columns}
                                                data={data}
                                                defaultSortField="vendorname"
                                                pagination
                                                // selectableRows
                                                filtering
                                            
                                            />
                                            </DataTableExtensions>
                                        </div>
                                          
                                        </div>    
                                      
                                    </div>
                                </div>
                            </div>
                  </div>
                  <div className="contentfooter">
                      <div className="text-right">
                          <button type="button" className="btn btn-cancel">Cancel</button>
                          <button type="button" className="btn btn-send-lg disabled">Create Product</button>
                          <button type="button" className="btn btn-send-lg disabled">Create & Add New</button>
                      </div>
                  </div>
              </div>

      
        
      </div>
      
    </div>
    <Modal size="md" show={show} onHide={handleClose}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Add Brand</Modal.Title>
  </Modal.Header>
  <Modal.Body>
  <div class="row clear-fix">
						<div class="col-sm-12">
							<div class="form-group">
								<label for="inputPassword" class=" col-form-label">Brand Name</label>
								<input type="text" class="form-control custom-form" placeholder="Enter Brand Name"/>
							</div>
						</div>
						<div class="col-sm-12 mb-3">
							  <label for="inputPassword" class=" col-form-label  text-left">Same Brand Product List</label>
							  <div class="float-right">
								<button type="button" class="btn-blue-border btn btn-primary" >+ Add Product</button>	
							  </div>
						</div>
						<div class="col-sm-12">
						<div class="form-group row">
							<div class="col-sm-6">
								 <label for="inputPassword" class=" col-form-label">Product Name/SKU</label>
								  <div class="">
									<input type="text" class="form-control custom-form" placeholder="Enter Product name/SKU"/>
								 </div>	
							</div>
							<div class="col-sm-6">
								 <label for="inputPassword" class=" col-form-label">Product Type</label>
								  <div class="">
                                  <Select options = {options} className="cusselect" />
									
								 </div>
							</div>
						</div>
						</div>
				</div>    
    {/* <Button variant=" my-3 upld-btn" onClick={handleShow2}>Upload</Button> */}

  </Modal.Body>
  <Modal.Footer>
    <Button variant="popcuslink bdr">Cancel</Button>
    <Button variant="primary popcus" onClick={handleClose}>Save</Button>
  </Modal.Footer>
</Modal>

<Modal size="sm" className=" delete-bg"show={show2} onHide={handleClose2}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Delete</Modal.Title>
  </Modal.Header>
  <Modal.Body>
  <div class="row clear-fix">
    <div class="col-sm-12">
      <p class="mb-3">Do you wish to delete the selected brand?</p>
    </div>
   
</div>


  </Modal.Body>
  <Modal.Footer>
    <Button variant="popcuslink bdr">cancel</Button>
    <Button variant="primary popcus yes-bg" onClick={handleClose2}>Yes</Button>
  </Modal.Footer>
</Modal>
</>
	);
}


export default VendorListsTable;
