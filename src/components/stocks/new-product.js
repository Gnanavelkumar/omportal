import React, { Component,  useState } from "react";
import Select from 'react-select';
import { Button ,Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';

function NewProduct()  {
    
const options = [
  { value: 'Select Product Type', label: 'Select Product Type' },
  { value: '1', label: 'Desk phone – Wired' },
  { value: '2', label: 'Desk phone - Wireless' },
  { value: '3', label: 'Hand phone – Wired' },
  { value: '4', label: 'Hand phone - Wireless' } 
];
const options2 = [
  { value: 'Euro (£)', label: 'Euro (£)' },
  { value: 'Available', label: '<10' },
  { value: 'Stock Out', label: '<30' },
  { value: 'Discontinued', label: '<50' } 
];
const options3 = [
  { value: 'Euro (£)', label: 'Euro (£)' },
  { value: 'Available', label: '<10' },
  { value: 'Stock Out', label: '<30' },
  { value: 'Discontinued', label: '<50' } 
];
const options4 = [
  { value: 'Select Vendor', label: 'Select Vendor' },
  { value: 'Available', label: '<10' },
  { value: 'Stock Out', label: '<30' },
  { value: 'Discontinued', label: '<50' } 
];

const [show, setShow] = useState(false);
const [show2, setShow2] = useState(false);
const [show3, setShow3] = useState(false);

const handleClose = () => setShow(false);
const handleShow = () => setShow(true);

const handleClose2 = () => setShow2(false);
const handleShow2 = () => setShow2(true);

const handleClose3 = () => setShow3(false);
const handleShow3 = () => setShow3(true);


return (
<>
            <div className="row clearfix">
              <div className="col-sm-12 p-0">
                <div className="user-icon-right-head">
                  <ul class="bread-crum-cus-call">
                      <li><a href="#" data-toggle="tab">Products</a></li>
                      <li><img src="http://localhost/urmy-acc/assets/images/User/bread-arrow.svg" class="bred-arrw" alt=""/></li>
                      <li class="active"><a href="#" data-toggle="tab">Add Product</a></li>
                  </ul>
                </div>
                <div className="centercontent">
                  <div className="contentheadlist">
                      <h5>Add New Product</h5>
                  </div>
                  <div className="center-container">
                      <div className="purchasenew">
                          <div className="row">
                              <div className="col-md-6">
                                  <label className="cuslable">Product Name </label>
                                  <input type="text" className="form-control custom-form" placeholder="Enter Product Name"/>
                                  <label className="cuslable mt-4">SKU Code </label>
                                  <input type="text" className="form-control custom-form err" placeholder="Enter Product SKU Code"/>
                                  <div class="invalid-feedback">Example invalid custom select feedback</div>
                              </div>
                              <div className="col-md-6">
                                  <label className="cuslable">Product Description</label>
                                  <textarea className="form-control custom-form" rows="6" id="comment" placeholder="Enter description about the product"></textarea>
                              </div>
                          </div>
                          <div className="row mt-4">
                            <div className="col-md-6">
                                <label className="cuslable">Product Type </label>
                                <Select options = {options} className="cusselect" />
                            </div>
                            <div className="col-md-6">
                              <div className="row">
                                <div className="col-md-3">
                                    <label className="cuslable">Buy Price </label>
                                    <Select options = {options2} className="cusselect" />
                                </div>
                                <div className="col-md-3">
                                    <label className="cuslable">&nbsp; </label>
                                    <input type="text" className="form-control custom-form" placeholder="Brand Name"/>
                                </div>
                                <div className="col-md-3">
                                    <label className="cuslable">Sell Price </label>
                                    <Select options = {options3} className="cusselect" />
                                </div>
                                <div className="col-md-3">
                                    <label className="cuslable">&nbsp; </label>
                                    <input type="text" className="form-control custom-form" placeholder="Brand Name"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row mt-4">
                            <div className="col-md-6">
                                <label className="cuslable">Brand </label>
                                <input type="text" className="form-control custom-form" placeholder="Enter Product SKU Code"/>
                            </div>
                            <div className="col-md-6">
                              <div className="row">
                                <div className="col-md-6">
                                    <label className="cuslable">Product Weight </label>
                                    <div class="input-group mb-3">
                                      <input type="text" className="form-control custom-form" placeholder="Enter Product SKU Code"/>
                                      <div class="input-group-append">
                                        <span class="input-group-text">Kgs</span>
                                      </div>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <label className="cuslable">Stocks </label>
                                    <input type="text" className="form-control custom-form" placeholder="Brand Name"/>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row mt-4">
                              <div className="col-md-6">
                                  <label className="cuslable">Vendor Name </label>
                                  <Select options = {options4} className="cusselect" />
                              </div>
                              <div className="col-md-6">
                              </div>
                          </div>
                          <div className="row mt-4">
                              <div className="col-md-7">
                                  <label className="cuslable">Add Product Image <span className="prdimgcount">0/5</span> </label>
                                  <div className="row align-items-center">
                                    <div className="col-sm-2">
                                      <div className="uploadedimg">
                                        <img src="images/deskphone.jpg" alt="" />
                                      </div>
                                    </div>
                                    <div className="col-sm-2">
                                      <div className="uploadedimg">
                                        <img src="images/deskphone.jpg" alt="" />
                                      </div>
                                    </div>
                                    <div className="col-sm-2">
                                      <div className="uploadedimg">
                                        <img src="images/deskphone.jpg" alt="" />
                                      </div>
                                    </div>
                                    <div className="col-sm-2">
                                      <div className="uploadedimg">
                                        <img src="images/deskphone.jpg" alt="" />
                                      </div>
                                    </div>
                                    <div className="col-sm-2">
                                      <button type="button" className="btn upld-btn">Upload</button>
                                    </div>
                                    <div className="col-sm-2 p-0">
                                      <button type="button" className="btn btn-remove"><span className="icon_delete"></span> Cancel</button>
                                    </div>
                                  </div>
                                  <div class="">
                                    <button class="btn upld-btn"  onClick={handleShow} type="submit">Upload</button>
                                    <button class="btn upld-btn"  onClick={handleShow2} type="submit">Upload</button>
                                  </div>
                              </div>
                              <div className="col-md-6">
                                  
                              </div>
                          </div>
                      </div>
                  </div>
                  <div className="contentfooter">
                      <div className="text-right">
                          <button type="button" className="btn btn-cancel">Cancel</button>
                          <button type="button" className="btn btn-send-lg disabled">Create Product</button>
                          <button type="button" className="btn btn-send-lg disabled">Create & Add New</button>
                      </div>
                  </div>
              </div>
              </div>
              
            </div>





      <Modal size="md" show={show} onHide={handleClose}  aria-labelledby="example-modal-sizes-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-lg"></Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <label class="myLabel">
                <input type="file" required/>
                <div className="cusupldsec">
                  <p className="cusupldsec-lg">Select Images to Upload</p>
                  <p className="cusupldsec-md">Or <span className="cblue">drag and drop</span> an image</p>
                  <p className="cusupldsec-sm">(800X400 0r larger recommended, up to 2MB each)</p>
                </div>
            </label>
        </Modal.Body>
      </Modal>

      <Modal size="md" show={show2} onHide={handleClose2} aria-labelledby="example-modal-sizes-title-lg">
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-lg" className="m-auto">Selected Images</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row align-items-center mb-3">
            <div className="col-sm-6">
              <div className="pop-sel-img">
                <div className="img-selupld">
                  <div class="phoneimage"><img src="images/deskphone.jpg" alt=""/></div>
                  <div className="img-selupl2">
                    <p className="img-selupl-name">Image_1.jpeg</p>
                    <p className="img-selupl-size">476KB</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="d-flex align-items-center justify-content-end">
                <div className="">
                  <Button variant="popcuslink leftbtn">Edit</Button>
                </div>
                <div className="">
                  <Button variant="btn-closeicn-sm"><span className="icon_close"></span></Button>
                </div>
              </div>
            </div>
          </div>
          <div className="row align-items-center mb-3">
            <div className="col-sm-6">
              <div className="pop-sel-img">
                <div className="img-selupld">
                  <div class="phoneimage"><img src="images/deskphone.jpg" alt=""/></div>
                  <div className="img-selupl2">
                    <p className="img-selupl-name">Image_1.jpeg</p>
                    <p className="img-selupl-size">476KB</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="d-flex align-items-center justify-content-end">
                <div className="">
                  <Button variant="popcuslink leftbtn">Edit</Button>
                </div>
                <div className="">
                  <Button variant="btn-closeicn-sm"><span className="icon_close"></span></Button>
                </div>
              </div>
            </div>
          </div>
          <div className="row align-items-center mb-3">
            <div className="col-sm-6">
              <div className="pop-sel-img">
                <div className="img-selupld">
                  <div class="phoneimage"><img src="images/deskphone.jpg" alt=""/></div>
                  <div className="img-selupl2">
                    <p className="img-selupl-name">Image_1.jpeg</p>
                    <p className="img-selupl-size">476KB</p>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-sm-6">
              <div className="d-flex align-items-center justify-content-end">
                <div className="">
                  <Button variant="popcuslink leftbtn">Edit</Button>
                </div>
                <div className="">
                  <Button variant="btn-closeicn-sm"><span className="icon_close"></span></Button>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="popcuslink leftbtn">Deselect All</Button>
          <Button variant="popcuslink" onClick={handleShow3}>Upload More</Button>
          <Button variant="primary popcus" onClick={handleClose}>Upload <span className="upldimgtot">3</span></Button>
        </Modal.Footer>
      </Modal>

      <Modal size="md" show={show3} onHide={handleClose3}  aria-labelledby="example-modal-sizes-title-lg">
        <Modal.Header closeButton><Button variant="popcuslink-back" onClick={handleClose3}><span className="icon_left_arrow1"></span></Button>
          <Modal.Title id="example-modal-sizes-title-lg" className="m-auto">Edit Image</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="container">
            <div className="row">
              <div className="col-sm-2">
                <div className="updlmoreimg">
                  <p className=""><span className="icon_crop"></span></p>
                  <p className="">Crop</p>
                </div>
                <div className="updlmoreimg">
                  <p className=""><span className="icon_rotation"></span></p>
                  <p className="">Rotate</p>
                </div>
              </div>
              <div className="col-sm-9">
                <div className="upldmoreicn-pop">
                  <img src="images/deskphone.jpg" alt="" />
                </div>
              </div>
              <div className="col-sm-1"></div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="popcuslink leftbtn">Reset</Button>
          <Button variant="primary popcus" onClick={handleClose}>Done</Button>
        </Modal.Footer>
      </Modal>
</>

	);
}


export default NewProduct;
