import React from "react";
import Button from 'react-bootstrap/Button'



export const columns = [
  {
    name: "Product Name",
    selector: "productname",
    sortable: true,
    cell: row => <div className="phoneimage"><img src="images/deskphone.jpg" alt=""/> <span className="phonename">Unify CP600</span></div>
  },
  {
    name: "SKU",
    selector: "sku",
    sortable: true
  },
  {
    name: "Stocks",
    selector: "stocks",
    sortable: true
  },
  {
    name: "Vendor",
    selector: "vendor",
    sortable: true
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
    cell: row => <span class="ordered">Ordered</span>
  },
  {
    name: "Buy Price",
    selector: "buyprice",
    sortable: true
  },
  {
    name: "Sell Price",
    selector: "sellprice",
    sortable: true
  },
  {
    name: "Weight",
    selector: "weight",
    sortable: true
  },
  {
    name: "Last Updated",
    selector: "lastupdated",
    sortable: true
  },
  {
    name: "Action",
    selector: "action",
    sortable: false,
    cell: row => <span className="icon_options actioneround f16"></span>,
  }
];

export const data = [
  {
    productname: 'UDE29382938283asd',
    sku: "SD345456",
    stocks: "1234",
    vendor: "Micromax Informatics",
    status: '02/04/2020',
    buyprice: "£18.64",
    sellprice: "£22.64",
    weight: "1.4kg",
    lastupdated: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },{
    productname: 'UDE29382938283asd',
    sku: "SD345456",
    stocks: "1234",
    vendor: "Micromax Informatics",
    status: '02/04/2020',
    buyprice: "£18.64",
    sellprice: "£22.64",
    weight: "1.4kg",
    lastupdated: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },{
    productname: 'UDE29382938283asd',
    sku: "SD345456",
    stocks: "1234",
    vendor: "Micromax Informatics",
    status: '02/04/2020',
    buyprice: "£18.64",
    sellprice: "£22.64",
    weight: "1.4kg",
    lastupdated: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },{
    productname: 'UDE29382938283asd',
    sku: "SD345456",
    stocks: "1234",
    vendor: "Micromax Informatics",
    status: '02/04/2020',
    buyprice: "£18.64",
    sellprice: "£22.64",
    weight: "1.4kg",
    lastupdated: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },{
    productname: 'UDE29382938283asd',
    sku: "SD345456",
    stocks: "1234",
    vendor: "Micromax Informatics",
    status: '02/04/2020',
    buyprice: "£18.64",
    sellprice: "£22.64",
    weight: "1.4kg",
    lastupdated: "02/04/2020",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  }];
