import React, { Component,  useState } from "react";
import { Link } from "react-router-dom";
import { Button ,Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';
import DataTable from "react-data-table-component";
import DataTableExtensions from 'react-data-table-component-extensions';
import {columns, data} from "../stocks/products-data";
import Select from 'react-select';
function ProductsMain()  {
    
const options = [
  { value: 'All', label: 'All' },
  { value: 'Available', label: 'Available' },
  { value: 'Stock Out', label: 'Stock Out' },
  { value: 'Discontinued', label: 'Discontinued' } 
];
const options2 = [
  { value: 'All', label: 'All' },
  { value: 'Available', label: '<10' },
  { value: 'Stock Out', label: '<30' },
  { value: 'Discontinued', label: '<50' } 
];



const [show, setShow] = useState(false);
const [show2, setShow2] = useState(false);

const handleClose = () => setShow(false);
const handleShow = () => setShow(true);

const handleClose2 = () => setShow2(false);
const handleShow2 = () => setShow2(true);




  const [hideDirector, setHideDirector] = React.useState(false);
    const tableData = {
        columns,
        data
      };

      

    const BootyCheckbox = React.forwardRef(({ onClick, ...rest }, ref) => (
      <div className="custom-control custom-checkbox">
        <input
          type="radio"
          className="custom-control-input"
          ref={ref}
          {...rest}
        />
        <label className="custom-control-label" onClick={onClick} />
      </div>
    ));
    const handleChange = (state) => {
    // You can use setState or dispatch with something like Redux so we can use the retrieved data
    console.log('Selected Rows: ', state.selectedRows);
    };
    
        return (
<>
  
    
 
            <div className="row clearfix">
              <div className="col-sm-12 p-0">
                <div className="user-icon-right-head">
                  <h4>Products <span className="outof">(128 of 128)</span></h4>
                  <div className="text-right"><button type="button" className="btn btn-primary mr-2">Add Product</button> <button type="button" className="btn btn-outline" onClick={handleShow}><span className="icon_upload"></span>Bulk Upload</button></div>
                </div>
                <div className="ctables">
                  <div className="filterrow">
                    <div className="col-sm-3 col-md-3">
                      <label className="cusselect-label">Status</label> <Select options = {options} className="cusselect" />
                    </div>
                    <div className="col-sm-3 col-md-3">
                      <label className="cusselect-label">Stocks</label> <Select options = {options2} className="cusselect"/>
                    </div>
                    <div className="col-sm-6 col-md-6 text-right pr-0">
                      <div className="fillterbtn">
                        <Button className="btn-blue-border" onClick={() => setHideDirector(!hideDirector)}>Download Report</Button>
                        <a href="" class="dtbtn-cus" data-toggle="modal" data-target="#delexport"><span className="icon_setting"></span> </a>
                      </div>
                    </div>
                  </div>
                  <DataTableExtensions {...tableData}>
                    <DataTable
                      columns={columns}
                      data={data}
                      defaultSortField="vendorname"
                      pagination
                      selectableRows
                      filtering
                      selectableRowsComponent={BootyCheckbox}
                    />
                  </DataTableExtensions>
                </div>
              </div>
              
            </div>
<Modal size="md" show={show} onHide={handleClose}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Bulk Upload product details</Modal.Title>
  </Modal.Header>
  <Modal.Body>
    <p>Make sure your .xls file has column headers for <b>Product name, SKU Code,Vendor Name, Status, Buy Price , Sell Price, and Weight…</b>to help ensure a successful upload.</p>
    <p><Button variant="download-tran"><span className="icon_download"></span> Download XLS Sample</Button></p>
    <p><label className="cuslabel">Select a XLS File <sup className="mand">*</sup></label></p>
    <Button variant=" my-3 upld-btn" onClick={handleShow2}>Upload</Button>

  </Modal.Body>
  <Modal.Footer>
    <Button variant="popcuslink bdr">Cancel</Button>
    <Button variant="primary popcus" onClick={handleClose}>Add</Button>
  </Modal.Footer>
</Modal>

<Modal size="md" show={show2} onHide={handleClose2}  aria-labelledby="example-modal-sizes-title-lg">
  <Modal.Header closeButton>
    <Modal.Title id="example-modal-sizes-title-lg">Bulk Upload product details</Modal.Title>
  </Modal.Header>
  <Modal.Body>
  <div class="row clear-fix">
    <div class="col-sm-12">
    <div class="form-group row">
      <div class="col-sm-6">
          <label for="inputPassword" class=" col-form-label">Import Summary</label>
            <div class="position-relative">
              <input type="text" class="form-control custom-form" placeholder="Errors"/>
              <span class="war-err-cunt">2</span>   
          </div>  
      </div>
    </div>
    </div>
    <div class="col-sm-12">
      <p class="mb-3">0 errors were found in your file. These errors are outlined in the .xls available for download below. The .XLS with errors will not be available for export once you exit this screen. You must report the .XLS with errors before proceeding.</p>
    </div>
    <div class="col-sm-12">
    <div class="form-group row">
      <div class="col-sm-6">
        <div class="position-relative">
            <input type="text" class="form-control custom-form" placeholder="Successful"/>
            <span class="suc-err-cunt">12</span> 
        </div>  
      </div>
    </div>
    </div>
</div>


  </Modal.Body>
  <Modal.Footer>
    <Button variant="popcuslink bdr">Export Errors</Button>
    <Button variant="primary popcus" onClick={handleClose2}>Add</Button>
  </Modal.Footer>
</Modal>
</>

	);
}


export default ProductsMain;
