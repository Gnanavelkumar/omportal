import React, { Component,  useState } from "react";
import Select from 'react-select';
import { Button ,Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';

function NewPurchaseProduct()  {
    
const options = [
  { value: 'Select Product Type', label: 'Select Product Type' },
  { value: '1', label: 'Desk phone – Wired' },
  { value: '2', label: 'Desk phone - Wireless' },
  { value: '3', label: 'Hand phone – Wired' },
  { value: '4', label: 'Hand phone - Wireless' } 
];
const options2 = [
  { value: 'Euro (£)', label: 'Euro (£)' },
  { value: 'Available', label: '<10' },
  { value: 'Stock Out', label: '<30' },
  { value: 'Discontinued', label: '<50' } 
];
const options3 = [
  { value: 'Euro (£)', label: 'Euro (£)' },
  { value: 'Available', label: '<10' },
  { value: 'Stock Out', label: '<30' },
  { value: 'Discontinued', label: '<50' } 
];
const options4 = [
  { value: 'Select Vendor', label: 'Select Vendor' },
  { value: 'Available', label: '<10' },
  { value: 'Stock Out', label: '<30' },
  { value: 'Discontinued', label: '<50' } 
];

const [show, setShow] = useState(false);
const [show2, setShow2] = useState(false);
const [show3, setShow3] = useState(false);

const handleClose = () => setShow(false);
const handleShow = () => setShow(true);

const handleClose2 = () => setShow2(false);
const handleShow2 = () => setShow2(true);

const handleClose3 = () => setShow3(false);
const handleShow3 = () => setShow3(true);


return (
<>
            <div className="row clearfix">
              <div className="col-sm-12 p-0">
                <div className="user-icon-right-head">
                  <ul class="bread-crum-cus-call">
                      <li><a href="#" data-toggle="tab">Products</a></li>
                      <li><img src="http://localhost/urmy-acc/assets/images/User/bread-arrow.svg" class="bred-arrw" alt=""/></li>
                      <li class="active"><a href="#" data-toggle="tab">Add Product</a></li>
                  </ul>
                </div>
                <div className="centercontent">
                  <div className="contentheadlist">
                      <h5>Add New Product</h5>
                  </div>
                  <div className="center-container">
                  <div class="purchasenew">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="cuslable">Product Name </label>
                                        <input type="text" class="form-control custom-form" placeholder="Enter Product Name" />
                                    </div>
                                    <div class="col-md-6">
                                        <label class="cuslable">Product Type </label>
                                        <input type="text" class="form-control custom-form" placeholder="Product Type" />
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        <label class="cuslable">SKU Code </label>
                                        <input type="text" class="form-control custom-form" placeholder="SKU Code" />
                                    </div>
                                    <div class="col-md-6">
                                        <label class="cuslable">Brand </label>
                                        <input type="text" class="form-control custom-form" placeholder="Brand Name" />
                                    </div>
                                </div>
                                <div class="row mt-4">
                                    <div class="col-md-6">
                                        <label class="cuslable">Vendor Name </label>
                                        <input type="text" class="form-control custom-form" placeholder="First Name" />
                                    </div>
                                    <div class="col-md-6">
                                        
                                    </div>
                                </div>
                            </div>
                  </div>
                  <div className="contentfooter">
                      <div className="text-right">
                          <button type="button" className="btn btn-cancel">Cancel</button>
                          <button type="button" className="btn btn-send-lg disabled">Send</button>
                      </div>
                  </div>
              </div>
              </div>
              
            </div>




</>

	);
}


export default NewPurchaseProduct;
