import React, { Component } from "react";
import Chart from "react-apexcharts";
import Select from 'react-select';

class App extends Component {
  
  constructor(props) {
    super(props);


    this.state = {
      
      options: {
        chart: {
          id: "basic-bar"
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          show: true,
          width: 1,
          colors: ['#147AD6', '#79D2DE']
        },
        fill: {
          colors: ['#1D76F2', '#D048A6']
        },
        xaxis: {
          categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
        },
        plotOptions: {
          bar: {
            columnWidth: '30%',
            endingShape: 'rounded'  
          }
        }
      },
      series: [
        {
          data: [80, 89, 47, 16, 14, 3, 60, 58]
        },
        {
          data: [22, 49, 57, 36, 34, 15, 80, 28]
        }
      ],
      
      yaxis: [
       
        {
          title: {
            text: "Net Sales"
          },
        },
        {
          opposite: true,
          title: {
            text: "Costs"
          }
        }
      ]
    };
  }

  render() {
    const options = [
      { value: 'Select Product Type', label: 'Last 7 Days ( 20Nov-27Nov )' },
      { value: '1', label: 'Today' },
      { value: '2', label: 'Last 7 Days' },
      { value: '3', label: 'This Week' },
      { value: '4', label: 'This Month' },
      { value: '4', label: 'Last Month' },
      { value: '4', label: 'This Year' },
      { value: '4', label: 'Last Year' },
      { value: '4', label: 'Custom' }
      ];
    return (
      <>
        
        <div className="row clearfix">
          <div className="col-sm-12 p-0">
            <div className="user-icon-right-head">
              <h4>Dashboard</h4>
            </div>
            <div className="centercontent">
              <div className="center-container">
                <div class="d-flex justify-content-between">
                  <div className="col-3"><Select options = {options} className="cusselect dashselect" /></div>
                  <div class="text-right"><button type="button" class="btn-blue-border">Download Report</button></div>
                </div>
                <div class="row mt-5">
                  <div className="col-sm-3 dashbox1">
                    <div class="panel-tail d-flex">
                      <div class="p-2">
                        <img src="images/total_company.svg" alt="user" class="cal-for-suc"/>
                      </div>
                      <div class="p-2 rig-pan-inn-text">
                        <h4>£ 12517</h4>
                        <p>Net Sales</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-3 dashbox2">
                    <div class="panel-tail d-flex">
                      <div class="p-2">
                        <img src="images/subscriptions.svg" alt="user" class="cal-for-suc"/>
                      </div>
                      <div class="p-2 rig-pan-inn-text">
                        <h4>£ 9517</h4>
                        <p>Cost of Goods Sold</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-3 dashbox3">
                    <div class="panel-tail d-flex">
                      <div class="p-2">
                        <img src="images/revenue.svg" alt="user" class="cal-for-suc"/>
                      </div>
                      <div class="p-2 rig-pan-inn-text">
                        <h4>£ 2517</h4>
                        <p>Gross profit</p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-3 dashbox4">
                    <div class="panel-tail d-flex">
                      <div class="p-2">
                        <img src="images/commission.svg" alt="user" class="cal-for-suc"/>
                      </div>
                      <div class="p-2 rig-pan-inn-text">
                        <h4>11879</h4>
                        <p>Stocks on Hand</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row mt-5">
                  <div className="col-sm-4">
                    <div class="chartbox">
                      <p className="chart-title">Revenue Vs Costs</p>
                      <Chart
                        options={this.state.options}
                        series={this.state.series}
                        type="line"
                        width="100%"
                      />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div class="chartbox">
                      <p className="chart-title">Total Profit</p>
                      <Chart
                        options={this.state.options}
                        series={this.state.series}
                        type="line"
                        width="100%"
                      />
                    </div>
                  </div>
                  <div className="col-sm-4">
                    <div class="chartbox">
                      <p className="chart-title">Orders</p>
                      <Chart
                        options={this.state.options}
                        series={this.state.series}
                        type="bar"
                        width="100%"
                      />
                    </div>
                  </div>
                </div>
              </div>
          </div>
          </div>
          
        </div>
      </>
    );
  }
}

export default App;