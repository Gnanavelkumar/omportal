import React, { Component,  useState } from "react";
import { Form } from 'react-bootstrap';

function NewPromotions()  {
    
return (
<>
            <div className="row clearfix">
              <div className="col-sm-12 p-0">
                <div className="user-icon-right-head">
                  <ul className="bread-crum-cus-call">
                      <li><a href="#" data-toggle="tab">Vendors</a></li>
                      <li><img src="http://localhost/urmy-acc/assets/images/User/bread-arrow.svg" className="bred-arrw" alt=""/></li>
                      <li className="active"><a href="#" data-toggle="tab">Create New Promotion</a></li>
                  </ul>
                </div>
                <div className="centercontent">
                  <div className="contentheadlist">
                      <h5>Create New Customer Promotion</h5>
                  </div>
                  <div className="center-container">
                      <div className="purchasenew">
                          <div className="row">
                              <div className="col-md-6">
                                  <label className="cuslable">Customer ID </label>
                                  <input type="text" className="form-control custom-form" placeholder="Enter Customer ID"/>
                              </div>
                              <div className="col-md-6">
                                <label className="cuslable">Discount (%) </label>
                                <div className="row">
                                  <div className="col-md-6">
                                    <div className="input-group mb-3">
                                      <input type="text" className="form-control custom-form" placeholder="Enter Discount Percentage"/>
                                      <div className="input-group-append">
                                        <span className="input-group-text">%</span>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div className="row mt-2">
                            <div className="col-md-6">
                                <label className="cuslable">Plan Purchased </label>
                                <input type="text" className="form-control custom-form" placeholder="Customer Purchased Plan"/>
                            </div>
                            <div className="col-md-6">
                              <div className="row">
                                <div className="col-md-6">
                                    <label className="cuslable">Start Date</label>
                                    <Form.Control type="date" name="startdate" className="form-control custom-form clndr" placeholder="Date of Birth" />
                                </div>
                                <div className="col-md-6">
                                    <label className="cuslable">End Date</label>
                                    <Form.Control type="date" name="enddate" className="form-control custom-form clndr" placeholder="Date of Birth" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row mt-4">
                            <div className="col-md-6">
                                <label for="basic-url" className="cuslable">Enter Promotion Code </label>
                                <div className="input-group mb-3">
                                  <div className="input-group-prepend">
                                    <span className="input-group-text" id="basic-addon3">Euro (£)</span>
                                  </div>
                                  <input type="text" className="form-control custom-form" id="basic-url" aria-describedby="basic-addon3" />
                                </div>
                            </div>
                            <div className="col-md-6">
                              <div className="row">
                                <div className="col-md-6">
                                    <label className="cuslable">Promotion Code </label>
                                    <div className="input-group mb-3">
                                      <input type="text" className="form-control custom-form" placeholder="Enter Promotion Code"/>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <label className="cuslable">&nbsp;</label>
                                    <button type="button" className="btn btn-validatebtn">Validate</button>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                  </div>
                  <div className="contentfooter">
                      <div className="text-right">
                          <button type="button" className="btn btn-cancel">Cancel</button>
                          <button type="button" className="btn btn-send-lg disabled">Create promotion</button>
                          <button type="button" className="btn btn-send-lg disabled">Create & Add New</button>
                      </div>
                  </div>
              </div>
              </div>
              
            </div>


</>

	);
}


export default NewPromotions;
