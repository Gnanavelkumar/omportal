import React from "react";
import Dropdown from 'react-bootstrap/Dropdown'
import ButtonGroup from 'react-bootstrap/ButtonGroup'


export const columns = [
  {
    name: "SKU",
    selector: "sku",
    sortable: true
  },
  {
    name: "Promotion Code",
    selector: "promotioncode",
    sortable: true
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
    cell: row => <span class="ordered">Ordered</span>
  },
  {
    name: "Product Type",
    selector: "producttype",
    sortable: true
  },
  {
    name: "Min Qty",
    selector: "minqty",
    sortable: true
  },
  {
    name: "Start Date",
    selector: "startdate",
    sortable: true
  },
  {
    name: "End Date",
    selector: "enddate",
    sortable: true
  },
  {
    name: "Discount",
    selector: "discount",
    sortable: true
  },
  {
    name: "Promotion Type",
    selector: "promotiontype",
    sortable: true
  },
  {
    name: "Action",
    selector: "action",
    sortable: false,
    cell: row => <Dropdown><Dropdown.Toggle id="dropdown-custom-1" variant="actioneround2"><div class="optionmore"><span className="icon_options"></span></div></Dropdown.Toggle><Dropdown.Menu className="upldimgdrpdwn"><Dropdown.Item eventKey="1"><span className="icon_view"></span>View</Dropdown.Item><Dropdown.Item eventKey="2" className="editlink"><span className="icon_edit"></span>Edit</Dropdown.Item><Dropdown.Item eventKey="3" className="deletelink" active><span className="icon_delete"></span>Delete</Dropdown.Item></Dropdown.Menu></Dropdown>,
  }
];

export const data = [
  {
    sku: "ALCO751",
    promotioncode: "welvec245",
    status: "status",
    producttype: "Deskphone-Wired",
    minqty: '10',
    startdate: "02/04/2020",
    enddate: "02/04/2020",
    discount: "10%",
    promotiontype: "Product Based",
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.status}</div></div>
  },{
    sku: "ALCO751",
    promotioncode: "welvec245",
    status: "status",
    producttype: "Deskphone-Wired",
    minqty: '10',
    startdate: "02/04/2020",
    enddate: "02/04/2020",
    discount: "10%",
    promotiontype: "Product Based",
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.status}</div></div>
  },{
    sku: "ALCO751",
    promotioncode: "welvec245",
    status: "status",
    producttype: "Deskphone-Wired",
    minqty: '10',
    startdate: "02/04/2020",
    enddate: "02/04/2020",
    discount: "10%",
    promotiontype: "Product Based",
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.status}</div></div>
  },{
    sku: "ALCO751",
    promotioncode: "welvec245",
    status: "status",
    producttype: "Deskphone-Wired",
    minqty: '10',
    startdate: "02/04/2020",
    enddate: "02/04/2020",
    discount: "10%",
    promotiontype: "Product Based",
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.status}</div></div>
  },{
    sku: "ALCO751",
    promotioncode: "welvec245",
    status: "status",
    producttype: "Deskphone-Wired",
    minqty: '10',
    startdate: "02/04/2020",
    enddate: "02/04/2020",
    discount: "10%",
    promotiontype: "Product Based",
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.status}</div></div>
  }];
