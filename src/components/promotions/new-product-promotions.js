import React, { Component,  useState } from "react";
import Select from 'react-select';
import { Button ,Modal, Tooltip, OverlayTrigger } from 'react-bootstrap';
import { Form } from 'react-bootstrap';
import Overlay from 'react-bootstrap/Overlay'
import Popover from 'react-bootstrap/Popover'

function NewProductPromotions()  {
    
const options = [
  { value: 'Select Product Type', label: 'Select Product Type' },
  { value: '1', label: 'Desk phone – Wired' },
  { value: '2', label: 'Desk phone - Wireless' },
  { value: '3', label: 'Hand phone – Wired' },
  { value: '4', label: 'Hand phone - Wireless' } 
];
const popover = (
  <Popover id="popover-basic">
    <Popover.Content>
      <p className="ttiptitle">Auto Apply</p>
      <p className="ttipdesc">Automatic – Based on the customer added products in the cart and quantity, promotion code applies automatically and show the amount minus discounted price to Pay</p>
    </Popover.Content>
  </Popover>
);

const Example = () => (
  <OverlayTrigger trigger="click" placement="bottom" overlay={popover}>
    <span className="icon_information ttotipicn"> </span>
  </OverlayTrigger>
);
return (
<>
            <div className="row clearfix">
              <div className="col-sm-12 p-0">
                <div className="user-icon-right-head">
                  <ul className="bread-crum-cus-call">
                      <li><a href="#" data-toggle="tab">Vendors</a></li>
                      <li><img src="http://localhost/urmy-acc/assets/images/User/bread-arrow.svg" className="bred-arrw" alt=""/></li>
                      <li className="active"><a href="#" data-toggle="tab">Create New Promotion</a></li>
                  </ul>
                </div>
                <div className="centercontent">
                  <div className="contentheadlist">
                      <h5>Create New Product Promotion</h5>
                  </div>
                  <div className="center-container">
                      <div className="purchasenew">
                          <div className="row">
                              <div className="col-md-6">
                                  <label className="cuslable">Product Name/SKU </label>
                                  <input type="text" className="form-control custom-form err" placeholder="Enter Customer ID"/>
                              </div>
                              <div className="col-md-6">
                                <div className="row">
                                  <div className="col-md-6">
                                      <label className="cuslable">Minimum Purchase</label>
                                      <input type="text" className="form-control custom-form" placeholder="Enter Minimum purchase"/>
                                  </div>
                                  <div className="col-md-6">
                                      <label className="cuslable">Discount (%)</label>
                                      <div className="input-group mb-3">
                                        <input type="text" className="form-control custom-form" placeholder="Enter Discount Percentage"/>
                                        <div className="input-group-append">
                                          <span className="input-group-text">%</span>
                                        </div>
                                      </div>
                                  </div>
                                </div>
                              </div>
                          </div>
                          <div className="row mt-2">
                            <div className="col-md-6">
                                <label className="cuslable">Product Type </label>
                                <Select options = {options} className="cusselect"/>
                            </div>
                            <div className="col-md-6">
                              <div className="row">
                                <div className="col-md-6">
                                    <label className="cuslable">Start Date</label>
                                    <Form.Control type="date" name="startdate" className="form-control custom-form clndr" placeholder="Date of Birth" />
                                </div>
                                <div className="col-md-6">
                                    <label className="cuslable">End Date</label>
                                    <Form.Control type="date" name="enddate" className="form-control custom-form clndr" placeholder="Date of Birth" />
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row mt-4">
                            <div className="col-md-6">
                              <div className="row">
                                <div className="col-md-6">
                                    <label className="cuslable">Promotion Code </label>
                                    <div className="input-group mb-3">
                                      <input type="text" className="form-control custom-form" placeholder="Enter Promotion Code"/>
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <label className="cuslable">&nbsp;</label>
                                    <button type="button" className="btn btn-validatebtn">Validate</button>
                                </div>
                                <div className="auto-apply col">
                                  <Example /> <span className="">Auto Apply</span>
                                  <input type="checkbox" hidden="hidden" id="username"/>
                                  <label className="switch ml-3" for="username"></label> <span className="swithsts ml-2">Enabled</span>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                  </div>
                  <div className="contentfooter">
                      <div className="text-right">
                          <button type="button" className="btn btn-cancel">Cancel</button>
                          <button type="button" className="btn btn-send-lg disabled">Create Promotion</button>
                          <button type="button" className="btn btn-send-lg disabled">Create & Add New</button>
                      </div>
                  </div>
              </div>
              </div>
              
            </div>


</>

	);
}


export default NewProductPromotions;
