import React, { Component } from "react";
import { Link } from "react-router-dom";
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown'
import MultiSelectTable from '../components/vendors/vendorslist';
import ButtonGroup from 'react-bootstrap/ButtonGroup'
class All extends Component {
  render() {

    return (

<>

<div class="container-fluid mt-5">
	<div class="row">
		<div class="col-md-3">
      <div class="custom-fileupload">
        <label class="cusfilelabel">
          <input type="file" required/>
          <span>Upload</span>
        </label>
      </div>
		</div>
		<div class="col-md-3">
    <span class="ordered">Ordered</span>

    <span class="received">Received</span>

    <span class="canceled">Canceled</span>
		</div>
		<div class="col-md-3">
			<div class="upld-btn-img">
				<label class="myFile" data-toggle="tooltip" data-placement="top" title="" data-original-title="Upload Invoice" aria-describedby="tooltip822075">
					<span className="icon_add"></span>
					<input type="file" />
				</label>
			</div>
		</div>
		<div class="col-md-3">
			<Dropdown as={ButtonGroup} drop="up">
				<Dropdown.Toggle drop="up" id="dropdown-custom-1" variant="actioneround2">
					<div class="updedimg-invoice dropup custom-drop-over">
						<img src="images/upd-invoice.PNG" />
					</div>	
				</Dropdown.Toggle>
				<Dropdown.Menu className="upldimgdrpdwn">
					<Dropdown.Item eventKey="1"><span className="icon_view"></span>View</Dropdown.Item>
					<Dropdown.Item eventKey="2" className="editlink"><span className="icon_edit"></span>Edit</Dropdown.Item>
					<Dropdown.Item eventKey="3" className="deletelink" active><span className="icon_delete"></span>Delete</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
		</div>
	</div>
</div>



<div class="container-fluid">
	<div class="row">
		<div class="col-md-8">
    <div class="succes-bar green">
        <span><img src="images/succestick.svg" alt="" class="logo-right-suc"/>UDE29382938283 Mail shared to Vendor Successfully <img src="images/close-pop.svg" alt="user" class="logo-close-pass mr-0" /></span>
    </div>
		</div>
	</div>
</div>
<br/>
<br/>
<br/>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8">
    <div class="succes-bar blue">
      <span><img src="images/succestick.svg" alt="" class="logo-right-suc" />Please enter correct product name </span>
    </div>
		</div>
	</div>
</div>

<br/>
<br/>
<br/>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8">
    <div class="succes-bar cancel">
      <span><span className="icon_correct toasttick"></span> Order Canceled Status Updated </span>
    </div>
		</div>
	</div>
</div>


<br/>
<br/>
<br/>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8">
    <div class="succes-bar cancel">
      <span><span className="icon_warning toasttick"></span> SKU is registered for another product</span>
    </div>
		</div>
	</div>
</div>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
		<div className="mb-2">
			<DropdownButton drop="up" variant="actioneround" title="">
				<Dropdown.Item eventKey="1">Action</Dropdown.Item>
				<Dropdown.Item eventKey="1">Action</Dropdown.Item>
				<Dropdown.Item eventKey="1">Action</Dropdown.Item>
				<Dropdown.Item eventKey="1">Action</Dropdown.Item>
			</DropdownButton>
				
			<br />
			<br />
			<br />
			<Dropdown as={ButtonGroup}>
				<Dropdown.Toggle id="dropdown-custom-1" variant="actioneround2">
					<div class="updedimg-invoice dropup custom-drop-over">
						<img src="images/upd-invoice.PNG" />
					</div>	
				</Dropdown.Toggle>
				<Dropdown.Menu className="upldimgdrpdwn">
					<Dropdown.Item eventKey="1"><span className="icon_view"></span>View</Dropdown.Item>
					<Dropdown.Item eventKey="2" className="editlink"><span className="icon_edit"></span>Edit</Dropdown.Item>
					<Dropdown.Item eventKey="3" className="deletelink" active><span className="icon_delete"></span>Delete</Dropdown.Item>
				</Dropdown.Menu>
			</Dropdown>
		</div>
		</div>
		<div class="col-md-3">

		
		</div>
		<div class="col-md-3">
		</div>
		<div class="col-md-3">
		</div>
	</div>
</div>


<div class="container-fluid">
	<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-3">
		</div>
		<div class="col-md-3">
		</div>
		<div class="col-md-3">
		</div>
	</div>
</div>
</>

    );
  }
}

export default All;
