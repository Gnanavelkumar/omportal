import React from "react";
import Button from 'react-bootstrap/Button'



export const columns = [
  {
    name: "Product Order ID",
    selector: "productid",
    sortable: true,
    cell: row => <span class="cblue">UDE29382938283</span>
  },
  {
    name: "Vendor",
    selector: "vendor",
    sortable: true
  },
  {
    name: "Product",
    selector: "products",
    sortable: true
  },
  {
    name: "SKU",
    selector: "sku",
    sortable: true
  },
  {
    name: "Last Updated",
    selector: "lastupdate",
    sortable: true
  },
  {
    name: "Status",
    selector: "status",
    sortable: true,
    cell: row => <span class="ordered">Ordered</span>
  },
  {
    name: "Invoice",
    selector: "invoice",
    sortable: true,
    cell: row => <div class="upld-btn-img">
    <label class="myFile" data-toggle="tooltip" data-placement="top" title="Upload Invoice">
        <span className="icon_add f16"></span>
        <input type="file" />
      </label>
</div>,
    
  },
  {
    name: "Action",
    selector: "action",
    sortable: false,
    cell: row => <span className="icon_options actioneround f16"></span>,
  }
];

export const data = [
  {
    productid: 'UDE29382938283asd',
    vendor: "Micromax Informatics",
    products: "Unify CP600 Desktop-Wired Brand Polycom",
    sku: "SD345456",
    lastupdate: '02/04/2020',
    status: "<span class='ordered'>Ordered</span>",
    invoice: "test",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>',
    cell: row => <div><div style={{ fontWeight: 700 }}>{row.productid}{row.vendor}{row.products}</div></div>
  },
  {
    productid: 'UDE29382938283',
    vendor: "Micromax Informatics",
    products: "Unify CP600 Desktop-Wired Brand Polycom",
    sku: "SD345456",
    lastupdate: '02/04/2020',
    status: "<span class='ordered'>Ordered</span>",
    invoice: "test",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>'
  },
  {
    productid: 'UDE29382938283',
    vendor: "Micromax Informatics",
    products: "Unify CP600 Desktop-Wired Brand Polycom",
    sku: "SD345456",
    lastupdate: '02/04/2020',
    status: "<span class='ordered'>Ordered</span>",
    invoice: "test",
    action:'<span class="actioneround"><i class="fa fa-eye" aria-hidden="true"></i></span>'
  }];
