import React, { Component } from "react";
import { Link } from "react-router-dom";
import {Dropdown, NavItem, Navbar, Button, Accordion, Card} from 'react-bootstrap';

class Header extends Component {
  render() {

    return (
  <>      
    <nav id="topNav" className="navbar navbar-expand-md fixed-top">
        <div className="col-md-2 p-0">
            <Link to=""><img src="../images/logo.svg" alt="Unified Ring" className="logo"/></Link>
        </div>
        
        <div className="collapse navbar-collapse" id="collapsingNavbar">
            <ul className="nav navbar-nav ml-auto">
                <li className="nav-item">
                    <a className="nav-link" href="#"><i className="fa fa-bell-o" aria-hidden="true"></i></a>
                </li>
            </ul>
            <div className="adminnotify">
                <p><span className="icon_notification"></span> <sup>2</sup></p>
            </div>
            <Dropdown className="admindpdwn">
                <Dropdown.Toggle id="dropdown-basic">
                    <div className="admindpdwntop">
                        <span className="fletter">JN</span>
                        <span className="ufirstname">Benjamin Adams <span className="usertitle">Finance</span></span>
                    </div>
                </Dropdown.Toggle>

                <Dropdown.Menu>
                    <Dropdown.Item href="#/action-1"><span className="icon_help f16"></span> Help</Dropdown.Item>
                    <Dropdown.Item href="#/action-2"><span className="icon_logout f16"></span> Logout</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </div>
    </nav>
    {/* <div className="headers">
        <div className="col-sm-3">
            <Link to=""><img src="../images/logo.svg" alt="Unified Ring" className="logo"/></Link>
        </div>
        <div className="col-sm-9">
            <div className="row clearfix toprights">
                <div className="col-sm-2">
                    <p><img src="../images/alaram.svg"/> <sup>2</sup></p>
                </div>
                <div className="col-sm-10">
                    <Dropdown className="admindpdwn">
                        <Dropdown.Toggle variant="success" id="dropdown-basic">
                            <div className="admindpdwntop">
                                <span className="fletter">JN</span>
                                <span className="ufirstname">Benjamin Adams <span className="usertitle">Finance</span></span>
                            </div>
                        </Dropdown.Toggle>

                        <Dropdown.Menu>
                            <Dropdown.Item href="#/action-1">Help</Dropdown.Item>
                            <Dropdown.Item href="#/action-2">Logout</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </div>
        </div>
    </div> */}
</>
    );
  }
}

export default Header;
