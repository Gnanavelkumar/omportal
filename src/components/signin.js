import React, { Component, useState } from "react";
import { Link } from "react-router-dom";

import { Button ,Fade, Form, OverlayTrigger } from 'react-bootstrap';

class Signin extends Component {
    render() {
  
    return (
      <>


<div className="container-fluid mainsignin">
    <div className="row clearfix">
        <div className="col-sm-6"></div>
        <div className="col-sm-4">
                
                <div className="login-page">
                    <img alt="" src="images/omlogo.svg" className="sign-logo"/>
                    
                    <div className="login-box">
                            <h4>Sign In to your Account</h4>

                            <h6 className="errheader" ><span className="icon-warning err-warning"></span><span className="errloginmsg">Please enter your valid email address or phone number.</span></h6>

                            <Form>
                                <Form.Group controlId="formBasicEmail">
                                    <Form.Control type="email" placeholder="Enter email" />
                                </Form.Group>

                                <Form.Group controlId="formBasicPassword">
                                    <Form.Control type="password" placeholder="Password" />
                                </Form.Group>


                                <div className="check-box row clearfix">
                                    <div className="col-sm-6">
                                        <Form.Group controlId="formBasicCheckbox">
                                            <Form.Check type="checkbox" label="Remember me" />
                                        </Form.Group>                   
                                    </div>
                                    <div className="col-sm-6 forgot">
                                        <p><Link to="/forgotpassword">Forgot Password?</Link></p>
                                    </div>
                                </div>
                                
                                <Button variant="primary" type="submit" className="sub-btn">
                                Sign In
                                </Button>
                            </Form>

                            <h5>Don’t have an account? <Link to="" target="_blank">Sign Up</Link></h5>

                    </div>
                

                   
            
                </div>
        </div>
        <div className="col-sm-2"></div>
    </div>
</div>

      </>
    );
  }
}

export default Signin;
